class Hangman
  attr_accessor :guesser, :referee, :board, :hangman

  def initialize(players = nil)
    if players == nil
      @players = {
        guesser: HumanPlayer.new,
        referee: ComputerPlayer.new
      }
    end
    @hangman = 0
    @guesser = @players[:guesser]
    @referee = @players[:referee]
  end

  def setup
    word_length = referee.pick_secret_word
    puts guesser.register_secret_length(word_length)
    @board = ""
    (1..word_length).each { @board += "_" }
    puts board
  end

  def take_turn
    guess = guesser.guess
    guess = guess.delete("\n")
    arr = referee.check_guess(guess)
    update_board(arr, guess)
    puts board
    if arr == []
      update_hangman
    end
    guesser.handle_response(arr, guess)
  end

  def update_board(arr, guess)
    arr.each  { |idx| board[idx] = guess }
  end

  def update_hangman
    @hangman += 1
    if @hangman == 1
      puts "your hangman is still in his cell."
    elsif @hangman == 2
      puts "your hangman is headed to the gallows."
    elsif @hangman == 3
      puts "your hangman is saying his last words."
    elsif @hangman == 4
      puts "your hangman has the noose around his neck."
    else
      puts "your hangman is executed."
    end
  end

  def win?
    board.each_char do |c|
      if c == "_"
        return false
      end
    end
    true
  end

  def play
    setup
    while @hangman < 5
      take_turn
      if win?
        puts "congratulations. sudden acuqittal."
        break
      end
    end
  end

end

class HumanPlayer

  def register_secret_length(word_length)
    "the length of the secret word is #{word_length}."
  end

  def guess
    puts "what letter would you like to guess?"
    $stdout.flush
    return gets
  end

  def handle_response(arr, guess)
    if arr == []
      puts "sorry, wrong letter."
    else
      puts "#{guess} shows up #{arr.length} times."
    end
  end

end

class ComputerPlayer

  attr_accessor :dictionary, :secret_word

  def initialize(dictionary = "lib/dictionary.txt")
    if dictionary == "lib/dictionary.txt"
      @dictionary = File.readlines("lib/dictionary.txt")
    else
      @dictionary = dictionary
    end
  end

  def pick_secret_word
    secret = dictionary.sample.chars
    secret.delete_at(-1)
    @secret_word = secret.join("")
    secret_word.length
  end

  def check_guess(ltr)
    answer = []
    secret_word.chars.each_with_index do |ch, i|
      if ch == ltr
        answer << i
      end
    end
    answer
  end

end

if $PROGRAM_NAME == __FILE__
  h = Hangman.new
  h.play
end
